+++

weight = 1

+++

{{% section %}}

# Quelques histoires…

… pour bien démarrer.

---

## Oups

{{% fragment %}}Appli Java & API REST déployée dans un Cloud{{% /fragment %}}

{{% fragment %}}Déploiement d'une nouvelle version{{% /fragment %}}

{{% fragment %}}Oups.{{% /fragment %}}

{{% note %}}

Dana a créé une super application Java exploitant un contrôleur REST qu'elle déploie au travers de la console
d'administration d'un Cloud provider.

Elle en fait la publicité auprès de ses connaissances, qui l'essaient et veulent s'en servir au quotidien.

Ses premiers utilisateurs ne tardent pas à lui demander d'ajouter des fonctionnalités. Emballée, elle développe la
première de ces fonctionnalités et déploie la nouvelle version de son application, toujours au travers de la console
d'administration de son Cloud provider.

Le résultat ne se fait pas attendre : les utilisateurs n'arrivent plus à utiliser l'application. Quelle pourrait en
être la raison ?

{{% /note %}}

---

## Pfff

{{% fragment %}}Petite appli Rust sur Raspberry Pi{{% /fragment %}}

{{% fragment %}}🐛 trouvé !{{% /fragment %}}

{{% fragment %}}Pfff.{{% /fragment %}}

{{% note %}}

Pedro s'est créé une petite application pour son usage personnel, il y a de ça quelques mois et l'a déployée sur un
Raspberry Pi dans un placard.

Aujourd'hui, il vient de découvrir un bogue dans son application et voudrait le corriger... sauf qu'il ne se souvient
plus du tout comment il avait déployé le tout sur le Raspberry Pi. Il finit par retrouver un tutoriel, mais il s'aperçoit
qu'il faudra d'abord faire monter de version plein de dépendances de son code, dont certaines sont maintenant
incompatibles. À votre avis, que va-t-il se passer ?

{{% /note %}}

---

## Argh


{{% fragment %}}Site web de location de chambres d'hôtes{{% /fragment %}}

{{% fragment %}}Données envolées + site détruit + fichiers vidéos douteux{{% /fragment %}}

{{% fragment %}}Argh !{{% /fragment %}}

{{% note %}}

Lisa a créé un petit site web pour gérer la location de chambres d'hôtes de ses beaux-parents.

Quelques mois plus tard, les beaux-parents se plaignent de ne plus pouvoir accéder à l'interface d'administration pour
définir des créneaux de réservation des chambres d'hôte.

Lisa constate que la base de données a été vidée et les fichiers du site remplacés par des fichiers vidéo aux noms
évocateurs. À votre avis, qu'a-t-il pu se passer ?

{{% /note %}}

{{% /section %}}