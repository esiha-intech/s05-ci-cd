+++

weight = 3

+++

# Deuxième Partie

GitLab CI/CD: premiers contacts

{{% note %}}
À votre avis, qu'est-ce que c'est ?
{{% /note %}}

---

{{% section %}}

# Concepts de base

Le vocabulaire

---

## Pipelines et étapes

```mermaid
flowchart LR
    Integration --> Delivery --> Deployment
```

{{% note %}}
L'exécution des étapes se fait en séquence dans un pipeline.
{{% /note %}}

---

## Tâches et exécuteurs

```mermaid
flowchart LR
    subgraph Integration
        test("Compile & test ⚙️"):::task
        doc("Generate documentation ⚙️"):::task
    end
    test & doc --> Delivery --> Deployment
    
    classDef task stroke:#f66
```

{{% note %}}
Les tâches au sein d'une étape sont exécutées en parallèle. Toutes les tâches d'une étape doivent être terminées pour
passer à l'étape suivante.
{{% /note %}}

---

## Artéfacts

Produits intermédiaires ou finaux d'une étape ou d'un pipeline.

---

## Cache

Espace de stockage visant à diminuer les temps de téléchargement de dépendances.

{{% /section %}}

---

{{% section %}}

# YAML

Yet Another Markup Language

---

## Les bases

```yaml
instruments: # Objet
  piano: # Sous-objet
    famille: "cordes frappées" # Propriété de type string
    variantes:
      - droit # Détection automatique : string
      - "à queue"
    touches: 88 # Détection automatique : nombre

  guitare:
    famille: cordes pincées
    variantes: [ classique, folk ]  # Syntaxe de liste alternative
    cordes: 6
```

{{% note %}}

* Fichiers de configuration
* Indentation
* Rédaction manuelle
* Comparaison avec JSON :
    * un peu moins verbeux
    * réutilisation de blocs
    * commentaires
    * chaînes multilignes
    * syntaxes alternatives
    * plus complexe à générer/lire pour un programme

{{% /note %}}

---

## Quelques éléments avancés

```yaml
instruments:
  piano: &piano # Définition d'ancre
    famille: "cordes frappées"
    variantes: [ "droit", "à queue" ]
    touches: 88

  guitare: &guitare
    famille: "cordes pincées"
    variantes: [ "classique", "folk" ]
    cordes: 6
    notes: >- # Chaîne multiligne fusionnée (>) sans fin de ligne (-)
      Il existe aussi des guitares avec plus de 6 cordes,
      voire des guitares à plusieurs manches.

  guitare basse:
    <<: *guitare # Inclusion pour redéfinition
    variantes: [ ]
    cordes: 4
    supérieur: true

encombrants: [ *piano ] # Utilisation d'ancre
```

{{% /section %}}

---

{{% section %}}

# Configuration

Pas de clic-bouton ici

---

## `.gitlab-ci.yml`

* Décrit la structure de votre pipeline
* Évolue avec le code, donc ajouté au VCS

---

## Exemple basique

```mermaid
flowchart LR
    subgraph Integration
        test("Compile & test ⚙️"):::task
        doc("Generate documentation ⚙️"):::task
    end
    test & doc --> Delivery --> Deployment

    classDef task stroke:#f66
```

```yaml
stages:
  - integration
  - delivery
  - deployment

compile-test:
  stage: integration
  script:
    - echo "⚙️ Compiling..."
    - echo "👓 Testing..."

generate-documentation:
  stage: integration
  script:
    - echo "Generating some documentation"
```

{{% /section %}}

---

# Travaux Pratiques

1. Créez un pipeline Gitlab CI/CD dans un projet à vous en vous basant sur l'exemple vu.
2. Ajoutez une tâche dans l'étape _delivery_ et une autre dans l'étape _deployment_.
3. Ajoutez à l'une des tâches une ligne de script `/usr/bin/false`.
