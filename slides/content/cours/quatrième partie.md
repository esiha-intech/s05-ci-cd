+++

weight = 5

+++

# Quatrième Partie

Sujets additionnels, mais capitaux.

---

{{% section %}}

# Shift left

Le terme à retenir pour briller en société.

---

## Principe

Déplacer plus tôt (vers « la gauche ») des considérations usuellement prises en compte tard dans le cycle de 
développement.

---

## Conséquences

* Raccourcit les boucles de rétroaction associées à ces considérations ;
* Favorise l'automatisation ;
* Favorise l'amélioration continue ;
* Favorise la prise de conscience de l'importance des considérations déplacées.

{{% /section %}}

---

{{% section %}}

# Dette technique

_« Un Lannister paie toujours ses dettes. »_

---

## Définition

Approximation permettant de livrer plus tôt, mais qui détériore la maintenabilité ou la validité du logiciel.

---

## Exemple 1

Utiliser des longueurs à vol d'oiseau pour comparer des itinéraires plutôt qu'une méthode plus proche de la réalité
dans une application de randonnée pédestre.

---

## Exemple 2

Compter les espaces dans une chaîne de caractères pour compter le nombre de mots.

{{% /section %}}

---

{{% section %}}

# Couverture de test

La malaimée, la dévoyée.

---

## Définition

Pourcentage du code de production atteint par le code de test.

---

## Précautions

La couverture de test ne permet pas de mesurer :

* la qualité des tests ;
* la pertinence des tests ;
* la maintenabilité des tests ;
* la complétude des tests.

---

## Utilité

L'évolution de la couverture de test est _une_ mesure de la santé d'une application.

---

## Couverture de test mutée

Variation mesurant la complétude des tests.

---

### Principe

1. Générer des variantes du code de production avec des mutations aux endroits clés ;
2. Lancer les tests pour chacune des variantes produites ;
3. Remonter toutes les mutations qui n'ont pas été détectées par les tests.

---

### Exemple

```java
class Validation {
    static boolean isValid(int n) {
        return n < 3;
    }
}
```

Mutations possibles :

* `return true;`
* `return n <= 3;`
* `return n > 3;`
* _etc._

---

### Mais pourquoi pas partout ?

Parce que le coût en temps et CPU explose vite. À réserver aux parties critiques du code.

{{% /section %}}

---

{{% section %}}

# Analyse qualité

Sortir de la CI avec une « qualité production ».

---

## Objectifs

* Rendre visible la dette technique accumulée ;
* Mesurer l'évolution de la couverture de test ;
* Mesurer l'évolution de la complexité du code ;
* Créer des seuils de qualité minimale.

---

### Complexe != compliqué

{{% note %}}
Quelle différence faites-vous entre complexe et compliqué ?
{{% /note %}}

---

### Seuil de qualité minimale

* À intégrer dans le pipeline après les tests ;
* S'il n'est pas bloquant, il est inutile.

{{% /section %}}

---

{{% section %}}

# Analyse sécurité

Boucher les trous, vite.

---

## Moyens

* Analyser le code source à la recherche de failles connues ;
* Analyser le dépôt de sources à la recherche de mots de passes et autres clés secrètes ;
* Rapprocher les dépendances des CVE déclarées.

{{% /section %}}

---

{{% section %}}

# Montées de versions

La majeure partie du travail, faite à votre place.

---

## Objectif

* Être au courant des évolutions proposées par les dépendances ;
* Profiter des corrections de bogues rapidement ;
* Appliquer rapidement les correctifs de sécurité.

---

## Revoilà Semver

Permet de distinguer automatiquement à quels types de mises à jour on a affaire.

{{% /section %}}

---

# Travaux Pratiques

1. Intégrez l'analyse du code par SonarCloud dans votre pipeline ;
2. Mettez en œuvre Renovate pour proposer des montées de version :
    * les versions PATCH sont fusionnées automatiquement après réussite des tests ;
    * les versions MINEUR et MAJEUR ne sont pas fusionnées automatiquement.

---

# Pour aller plus loin

* 12 factor apps ;
* Conventional commits ;
* Releases + tag de versions automatiques ;
* Création d'environnements éphémères.

---

{{% section %}}

# Conclusion

Et si on créait une boucle de rétroaction ?

---

## Oups

Appli Java & API REST déployée dans un Cloud

Déploiement d'une nouvelle version

Oups.

{{% note %}}
Quels concepts et outils vous paraissent pertinents pour éviter cette situation ?
{{% /note %}}

---

## Pfff

Petite appli Rust sur Raspberry Pi

🐛 trouvé !

Pfff.

{{% note %}}
Quels concepts et outils vous paraissent pertinents pour éviter cette situation ?
{{% /note %}}

---

## Argh


Site web de location de chambres d'hôtes

Données envolées + site détruit + fichiers vidéos douteux

Argh !

{{% note %}}
Quels concepts et outils vous paraissent pertinents pour éviter cette situation ?
{{% /note %}}

{{% /section %}}
