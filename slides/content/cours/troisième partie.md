+++

weight = 4

+++

# Troisième Partie

Application plus réaliste.

---

{{% section %}}

# Images et conteneurs

🐳

---

## Isolation

Chacun dans son coin… mais pour quoi faire ?

{{% note %}}
Selon vous, quelles pourraient être les raisons à vouloir faire en sorte que des programmes qui s'exécutent sur une même
machine soient le plus isolés les uns des autres ?

* Éviter des incompatibilités de bibliothèques tierces
* Limiter le risque qu'un programme s'intéresse de trop près à ce que font d'autres
* Pouvoir contrôler les quantités de ressources utilisées par un programme (CPU, RAM, disque, réseau)

{{% /note %}}

---

## Principe

* 1 conteneur = 1 processus
* Contrôler ce processus grâce aux mécaniques offertes par le noyau Linux pour :
  * Lui faire croire qu'il est seul sur la machine
  * Limiter son accès aux ressources (quotas CPU, RAM et disque, ports ouverts)

---

## 🐳 Docker

Encapsulation des mécaniques offertes par le noyau Linux pour offrir une création facilité de conteneurs.

---

## 🦦 Podman

Implémentation Open Source des standards _Open Container Initiative_ (OCI). Ne nécessite pas d'être root.

{{% note %}}
Dans la suite, pour « image Docker », comprendre « image OCI ».
{{% /note %}}

---

## Images

Ce sont des modèles de conteneur.

---

Une image est construite en couches successives qui permettent principalement de :

* modifier le système de fichier mis à disposition ;
* définir des ouvertures de ports ;
* définir quel sera le point d'entrée pour l'exécution des conteneurs ;
* définir des étiquettes associées.

---

### `Dockerfile`

Décrit comment construire une image.

```dockerfile
FROM docker.io/openjdk:17-slim
LABEL maintainer="team@company.com"
WORKDIR /opt/
EXPOSE 8080
ENTRYPOINT ["java"]
CMD ["-jar", "server.jar"]
COPY ./target/server-*-jar-with-dependencies.jar ./server.jar
```

{{% note %}}

* Chaque instruction correspond à une couche, à l'exception de `FROM` qui tire toutes les couches de l'image de base.
* Pour diminuer les temps de construction et l'espace disque occupé par les images, les couches existantes sont
  réutilisées autant que possible.
* L'identifiant d'une couche dépend de l'instruction qui la compose et de toutes les couches précédentes.

{{% /note %}}

---

### Construction de l'image

```shell
$ mvn package
[...]
[INFO] BUILD SUCCESS
[...]

$ ls target/*.jar
target/server-0.0.1.jar  target/server-0.0.1-jar-with-dependencies.jar

$ docker build -t my-server:0.0.1 .
Sending build context to Docker daemon  15.12MB
Step 1/7 : FROM docker.io/openjdk:17-slim
 ---> 37cb44321d04
Step 2/7 : LABEL maintainer="team@company.com"
 ---> Running in 6713a77a987e
Removing intermediate container 6713a77a987e
 ---> ae4782a6bf9d
Step 3/7 : WORKDIR /opt/
 ---> Running in 675ea99a0b0d
Removing intermediate container 675ea99a0b0d
 ---> e962c26ceb35
Step 4/7 : EXPOSE 8080
 ---> Running in a746ee883bf7
Removing intermediate container a746ee883bf7
 ---> ca6950753bfe
Step 5/7 : ENTRYPOINT ["java"]
 ---> Running in daea2183ccd6
Removing intermediate container daea2183ccd6
 ---> 1a62221c131a
Step 6/7 : CMD ["-jar", "server.jar"]
 ---> Running in efb34e27dd96
Removing intermediate container efb34e27dd96
 ---> 8e15b40becb9
Step 7/7 : COPY ./target/server-*-jar-with-dependencies.jar ./server.jar
 ---> 107c1b827b9e
Successfully built 107c1b827b9e
Successfully tagged my-server:0.0.1
```

---

### Lister les images

```shell
$ docker image ls
REPOSITORY   TAG       IMAGE ID       CREATED              SIZE
my-server    0.0.1     107c1b827b9e   About a minute ago   422MB
openjdk      17-slim   37cb44321d04   4 months ago         408MB
```

---

## Conteneurs

Ce sont des instanciations d'une image.

---

### Lancer un conteneur

```shell
$ docker run -d my-server:0.0.1
5aa7cb7a1d83dd3d3cc3a8c5a93db0ee0f28903c1ea068d9eb7d28ba0433ff28

$ docker ps
CONTAINER ID   IMAGE             COMMAND                  CREATED        STATUS        PORTS                    NAMES
5aa7cb7a1d83   my-server:0.0.1   "java -jar server.jar"   1 second ago   Up 1 second   0.0.0.0:49153->8080/tcp  
strange_spence

$ curl http://0.0.0.0:49153
Hello World!
```

---

### Arrêter un conteneur

```shell
$ docker stop strange_spence

$ docker ps -a
CONTAINER ID   IMAGE                COMMAND                  CREATED         STATUS                      PORTS    NAMES
5aa7cb7a1d83   my-server:0.0.1      "java -jar server.jar"   2 minutes ago   Exited (143) 2 seconds ago           
strange_spence

$ curl http://0.0.0.0:49153
curl: (7) Failed to connect to 0.0.0.0 port 49153 after 0 ms: Connexion refusée
```

---

## Étiquettes

`latest` par défaut.

---

### Besoin

* Différencier les variantes d'une image :
  * version ;
  * image de base ;
  * dépendances .

---

### ⚠ `latest` n'est pas votre ami

Ne pas utiliser de dépendances `latest` !

{{% note %}}
À votre avis, pourquoi est-ce généralement une mauvaise idée d'utiliser des images `latest` ?

Vous ne maîtrisez pas quand la dépendance est mise à jour.

{{% /note %}}

---

### Utilisation

```shell
$ docker tag my-server:0.0.1 esiha/my-server:0.0.1
```

---

## Registres

Distribuer des images

---

### Besoin

* Rendre accessible au reste de mon infrastructure une image pour la déployer ;
* Proposer des installations clé-en-main de mon logiciel ;
* Permettre aux autres développeurs de réutiliser mon logiciel comme base.

---

### Possibilités

* Docker Hub ;
* Quay (Red Hat) ;
* GitLab ;
* _etc._

---

### Publication sur Docker Hub

```shell
$ docker login
Username: esiha
Password: [masqué]

$ docker push esiha/my-server:0.0.1
The push refers to repository [docker.io/esiha/my-server]
c0a5bed7124a: Pushed
6be690267e47: Pushed
13a34b6fff78: Pushed
9c1b6dd6c1e6: Pushed
0.0.1: digest: sha256:df3e40521b820b521eab204048540ebf59cf2b22ce81ab59dd08c97010eac704 size: 1165

```

{{% /section %}}

---

{{% section %}}

# Semantic Versioning

_Semver_ pour les intimes.

---

## Semver en 1 diapositive

`MAJEUR.MINEUR.PATCH`

{{% note %}}
* Le numéro MAJEUR doit être incrémenté quand des changements d'API incompatibles sont introduits ;
* Le numéro MINEUR doit être incrémenté quand des fonctionnalités sont ajoutées en gardant la rétro-compatibilité ;
* Le numéro PATCH doit être incrémenté quand des corrections d'anomalies sont effectuées en gardant la 
  rétro-compatibilité.
{{% /note %}}

---

### Changement d'API incompatible

`2.4.5` → `3.0.0`

---

### Ajout de fonctionnalités

`2.4.5` → `2.5.0`

---

### Correction d'anomalies

`2.4.5` → `2.4.6`

---

## Intérêt

* Différencier les montées de versions bénignes de celles dangereuses ;
* Permet l'automatisation de montées de versions.

---

## Cas particuliers

Pour notre bien à tous.

---

### Versions `0.y.z`

Dénotent d'une API encore non stabilisée, des changements incompatibles peuvent être introduits sans changer le 
numéro MAJEUR.

{{% note %}}
Principalement utile pour les premières itérations du développement, où tout bouge très rapidement.
{{% /note %}}

---

### Préversions `-suffix`

`1.0.0-alpha` est une préversion de `1.0.0` et donc moins prioritaire.

---

### Métadonnées `+suffix`

`1.0.0+c7d8cee5` est la version `1.0.0`.

---

## Spécification complète

https://semver.org/

{{% /section %}}

---

{{% section %}}

# GitLab CI/CD, revisité

La clef de la flexibilité.

---

## Spécifier l'image à utiliser `.gitlab-ci.yml`

```yaml
image: "docker.io/openjdk:11-slim" # image par défaut pour toutes les tâches

integration:
  image: "docker.io/maven:3-openjdk-11-slim" # image spécifique à la tâche "integration"
```

---

## Variables

| Nom                     | Utilité                              |
|-------------------------|--------------------------------------|
| `$CI_REGISTRY`          | URL du registre d'images OCI intégré |
| `$CI_REGISTRY_USER`     | Nom d'utilisateur associé            |
| `$CI_REGISTRY_PASSWORD` | Mot de passe associé                 |

{{% note %}}
Il existe un grand nombre de variables prédéfinies pour vos pipelines.

Vous pouvez aussi définir les vôtres dans un bloc `variables`.
{{% /note %}}

---

## Artéfacts

Permettent de passer des « choses » d'une tâche à une autre.

```yaml
integration:
  artifacts:
    paths:
      - target/*.jar

delivery:
  dependencies:
    - integration # Les artéfacts de "integration" sont nécessaires pour "delivery".
```

---

# Travaux Pratiques

1. Activez le registre d'images si nécessaire dans votre projet GitLab.
2. Créez un `Dockerfile` qui permette de lancer votre application.
3. Configurez votre `.gitlab-ci.yml` pour créer l'image OCI et la pousser sur le registre interne GitLab.

{{% /section %}}
