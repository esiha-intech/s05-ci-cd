+++

weight = 2

+++

# Première Partie

Les bases

---

{{% section %}}

# Boucle de rétroaction

Feedback loop pour les anglophones.

---

## Touchez votre nez.

{{% note %}}
Comment savez-vous que vous l'avez touché ?
{{% /note %}}

---

## Test-Driven Development

Rouge → Vert → Réfection → Rouge…

---

*Une boucle de rétroaction plus courte est généralement plus utile qu'une plus longue.*

{{% /section %}}

---

{{% section %}}

# Continuous Integration

Le CI dans CI/CD

---

## But

Valider automatiquement l'absence de régression à chaque modification du code source.

---

## Prérequis

1. Utiliser un gestionnaire de sources (VCS)
2. Pousser fréquemment les modifications
3. Avoir des tests « d'intégration »

---

### Gestionnaire de sources

* Gestion de l'historique des modifications
* Travail collaboratif

---

### Pousser fréquemment les modifications

Incite à travailler par petits incréments.

{{% fragment %}}⇒ limite la portée des modifications à analyser en cas de régression{{% /fragment %}}

{{% fragment %}}⇒ limite le risque de construire sur une application cassée{{% /fragment %}}

{{% fragment %}}⇒ fournit une boucle de rétroaction courte{{% /fragment %}}

---

### Avoir des tests « d'intégration »

Valident que les différents composants de l'application fonctionnent bien ensemble.

{{% fragment %}}⇒ ne remplacent pas les tests « unitaires »{{% /fragment %}}

{{% fragment %}}⇒ doivent s'exécuter rapidement pour une boucle de rétroaction courte{{% /fragment %}}

---

## Que faudrait-il pour…

Valider automatiquement l'absence de régression à chaque modification du code source ?

{{% note %}}
Selon vous, quels sont les éléments et actions nécessaires pour atteindre ce but ?

* un outil qui surveille les modifications sur le VCS
* une configuration de comment compiler et tester l'application
* un moyen de connaître le résultat de l'intégration
* avoir accès à la liste des tests échoués et les raisons
* (un historique des intégrations passées)
  {{% /note %}}

---

## Les écueils à connaître

Les petites lignes du contrat.

---

### Rendez visible les échecs

Une CI qui ne vous crie pas à la figure « c'est cassé » est inutile et finira abandonnée.

{{% note %}}
E-mail, slack, discord, etc. le bon canal est celui qu'utilise l'équipe au quotidien.
{{% /note %}}

---

### Soignez la fiabilité des tests

Des tests souvent en faux positif, ce sont des tests inutiles qui vous feront abandonner la CI.

{{% note %}}
La gestion du temps et les exécutions concurrentes sont les plus délicates à traiter.
{{% /note %}}

---

### Maîtrisez la durée d'exécution

Une CI trop longue est une douleur qui vous fera l'abandonner.

{{% note %}}
Toujours cette histoire de boucle de rétroaction. Plus c'est court, mieux c'est !
{{% /note %}}

{{% /section %}}

---

{{% section %}}

# Fiabilité des Tests

Une question de responsabilités

---

## Rappel sur Dependency Inversion Principle (DIP)

Le D de SOLID.

---

### Définition

---

1. Des modules de haut niveau ne devraient pas dépendre de modules de bas niveau. Les deux devraient dépendre d'abstractions.

---

2. Les abstractions ne devraient pas dépendre des détails. Les détails devraient dépendre des abstractions.

---

### Exemple

---

Violation du DIP

```java
final class Lamp {
    void turnOn() {
        // Turns the lamp on.
    }

    void turnOff() {
        // Turns the lamp off.
    }
}

final class Button {
    private final Lamp lamp;

    Button(Lamp lamp) {
        this.lamp = lamp;
    }

    void detect() {
        if (isPressed()) {
            lamp.turnOn();
        } else {
            lamp.turnOff();
        }
    }

    private boolean isPressed() {
        // Tells whether it has been pressed.
    }
}
```

---

Inversion des dépendances

```java
interface Lamp {
    void turnOn();

    void turnOff();
}

final class FloodLight implements Lamp {
    @Override
    public void turnOn() {
        // Turns the floodlight on.
    }

    @Override
    public void turnOff() {
        // Turns the floodlight off.
    }
}

final class Button {
    private final Lamp lamp;

    Button(Lamp lamp) {
        this.lamp = lamp;
    }

    void detect() {
        if (isPressed()) {
            lamp.turnOn();
        } else {
            lamp.turnOff();
        }
    }

    private boolean isPressed() {
        // Tells whether it has been pressed.
    }
}
```

{{% note %}}
L'injection de dépendances implémentée au travers du constructeur de `Button` et l'interface `Lamp` permet de mettre en
œuvre l'inversion de dépendances.
{{% /note %}}

---

Exploitez le DIP pour améliorer la fiabilité en rendant explicite des dépendances implicites.

Exemple : obtenir l'heure courante est matérialisable par l'interface `Clock`.

---

## Écrivez des tests indépendants

{{% fragment %}}Faites en sorte que l'état initial soit reproductible.{{% /fragment %}}

{{% fragment %}}Faites en sorte que les tests puissent être exécutés dans n'importe quel ordre.{{% /fragment %}}

---

## Basez-vous sur le contrat, pas les détails

Un bon test se concentre sur le « quoi », pas sur le « comment ».

{{% /section %}}

---

{{% section %}}

# Continuous Delivery

Le CD de CI / CD

---

## But

Produire automatiquement un ou des paquets livrables à chaque modification de code source.

---

## Prérequis

Mettre en place et suivre les principes de la CI

---

## Que faudrait-il pour…

Produire automatiquement un ou des paquets livrables à chaque modification de code source ?

{{% note %}}
Selon vous, quels sont les éléments et actions nécessaires pour atteindre ce but ?

* un outil qui permette de déclencher la fabrication d'un paquet à la suite de la CI
* une configuration de comment créer les livrables
* un endroit où déposer les livrables
* (un historique des livrables produits et à partir de quelle version des sources)

{{% /note %}}

---

## Les écueils à connaître

Les autres petites lignes du contrat.

---

### Ne construisez qu'une fois les livrables

Tirez parti des caches de compilation pour gagner du temps et de la confiance dans vos livrables.

---

### Les dépendances font souvent partie des livrables applicatifs

Votre livrable n'est complet que s'il est autosuffisant là où il sera exécuté.

---

### La documentation est aussi un livrable

Souvent vue comme le vilain petit canard, la documentation est pourtant celle qui profite le plus du CD.

{{% /section %}}

---

{{% section %}}

# Continuous Deployment

L'autre CD de CI / CD

---

## But

Déployer automatiquement des livrables jusqu'aux utilisateurs finaux.

---

## Prérequis

1. Mettre en place et suivre les principes du Continuous Delivery
2. Avoir des tests « de démarrage » ou _smoke tests_.

---

### Tests « de démarrage » ou _smoke tests_

Ils consistent à s'assurer automatiquement de l'utilisabilité d'un livrable fraîchement déployé.

{{% note %}}
L'origine provient du test de matériel électronique : s'il dégage de la fumée à l'allumage, il faut couper le courant.
{{% /note %}}

---

#### Exemple

Pour une application REST, un endpoint dédié répond que toutes les dépendances externes ont pu être contactées.

---

## Que faudrait-il pour…

Déployer automatiquement des livrables jusqu'aux utilisateurs finaux ?

{{% note %}}
Selon vous, quels sont les éléments et actions nécessaires pour atteindre ce but ?

* un outil qui permette de déclencher des déploiements suite à la mise à disposition de livrables
* une configuration d'où effectuer les déploiements et comment déclencher les _smoke tests_
* (un historique des déploiements réalisés, avec quels livrables)

{{% /note %}}

---

## Les écueils à connaître

Encore ces fameuses petites lignes…

---

### La rétro-compatibilité est reine

Devoir livrer des applications dans un ordre précis enlève presque tout intérêt au Continuous Delivery.

---

### Rendez visible les échecs de _smoke tests_

Sinon, vous avez une recette parfaite pour casser votre production sans vous en apercevoir.

---

### Les retours arrière automatiques sont *souvent* une bonne idée

Sauf dans les cas où des modifications dans le stockage des données les rend non rétro-compatible.

---

### Les interruptions de service sont votre ennemi

Avoir une coupure de service de 10 minutes à chaque déploiement annule toute l'utilité du Continuous Delivery.

{{% /section %}}

---

{{% section %}}

# Récapitulation

Les bases

---

## Boucle de rétroaction

* Permet l'évaluation de la portée de vos actions.
* Généralement, plus elle est courte, plus elle est efficace.

---

## Continuous Integration

* Diminue le coût de l'intégration en la faisant le plus fréquemment possible.
* Nécessite d'utiliser :
  * Un gestionnaire de sources ;
  * Pousser fréquemment ;
  * Des tests « d'intégration ».

---

### ⚠ Points d'attention

* Rendre visible les échecs ;
* Fiabiliser les tests ;
* Maîtriser le temps d'exécution.

---

## Fiabilité des tests

* Le Dependency Inversion Principle est votre ami ;
* Indépendance ;
* Le contrat est le point de jonction.

---

## Continuous Delivery

* Diminue le coût d'obtention d'un livrable utilisable en le faisant le plus fréquemment possible.
* S'appuie sur les principes de CI.

---

### ⚠ Points d'attention

* Construire les livrables une seule fois ;
* Les dépendances peuvent faire partie des livrables ;
* La documentation est aussi un livrable.

---

## Continuous Deployment

* Diminue le Time-to-Market (TTM) en déployant le plus fréquemment possible.
* Nécessite d'utiliser :
  * Le Continuous Delivery ;
  * Des tests « de démarrage » ou _smoke tests_ .

---

### ⚠ Points d'attention

* Préserver la rétro-compatibilité ;
* Rendre visible les échecs ;
* Penser « retour arrière »;
* Traquer les interruptions de service.

{{% /section %}}